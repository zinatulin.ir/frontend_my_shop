import React from 'react';
import './styles/App.css';
import Menu from './Menu';
import { connect } from 'react-redux'


const App = ({store}) => {
  return (
    <div>
    	<Menu authorized={store.account.authorized} />
    </div>
  );
}

export default connect(
  state => ({
    store: state
  }),
  dispatch => ({})
)(App);
