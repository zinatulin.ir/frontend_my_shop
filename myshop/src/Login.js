import React from 'react';
import './styles/Login.css';
import Menu from './Menu';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';


const Login = ({store, onLogin}) => {
	let username = '';
	let password = '';

	const submit = () => {
    console.log("I'm here!!!!")
    onLogin(username.value, password.value);
    window.location.assign('/');
  }

  return (
  	<div>
  		<div>
  			<Menu authorized={store.account.authorized}/>
  		</div>
  		<div className="login">
    		<input type="text" placeholder="Логин" id="username" ref={(input) => { username = input }}/>  
  			<input type="password" placeholder="Пароль" id="password" ref={(input) => { password = input }}/>  
  			<Link to="/registration" className="registration">зарегистрироваться </Link>
  			<input type="submit" onClick={submit} value="Войти"/>
		  </div>
	  </div>
	);
}


export default connect(
  state => ({
    store: state
  }),
  dispatch => ({
  	onLogin: (username, password) => {
      console.log("in login")
      dispatch({ type: 'LOGIN', username: username, password: password});
    }
  })
)(Login);
