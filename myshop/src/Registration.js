import React from 'react';
import './styles/Registration.css';
import Menu from './Menu';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';


const Registration = ({store}) => {
  return (
    <div>
      <div>
        <Menu authorized={store.authorized} />
      </div>

<div id="registration-form">
  <div class='fieldset'>
    <legend>Регистрация</legend>
    <form action="#" method="post" data-validate="parsley">
      <div class='row'>
        <label for='firstname'>First Name</label>
        <input type="text2" placeholder="First Name" name='firstname' id='firstname' data-required="true" data-error-message="Your First Name is required" />
      </div>
      <div class='row'>
        <label for="email">E-mail</label>
        <input type="text2" placeholder="E-mail"  name='email' data-required="true" data-type="email" data-error-message="Your E-mail is required" />
      </div>
      <div class='row'>
        <label for="cemail">Confirm your E-mail</label>
        <input type="text2" placeholder="Confirm your E-mail" name='cemail' data-required="true" data-error-message="Your E-mail must correspond" />
      </div>
      <input type="submit2" value="Войти" />
    </form>
  </div>
</div>

  	</div>
  );
}


export default connect(
  state => ({
    store: state
  }),
  dispatch => ({
  	//onLogin: (username, password) => {
    //  dispatch({ type: 'LOGIN', username: username, password: password});
    //}
  })
)(Registration);