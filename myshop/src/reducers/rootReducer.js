import { combineReducers } from 'redux';

import account from './account';
//import your reducer:
//import simpleReducer from './simpleReducer';


export default combineReducers({
	account,
	//write name your reducer here
	//simpleReducer
});